class ContentModulesController < ApplicationController
  before_action :set_content_module, only: [:show, :edit, :update, :destroy]

  # GET /content_modules
  # GET /content_modules.json
  def index
    @content_modules = ContentModule.all
  end

  # GET /content_modules/1
  # GET /content_modules/1.json
  def show
  end

  # GET /content_modules/new
  def new
    @content_module = ContentModule.new
  end

  # GET /content_modules/1/edit
  def edit
  end

  # POST /content_modules
  # POST /content_modules.json
  def create
    @content_module = ContentModule.new(content_module_params)

    respond_to do |format|
      if @content_module.save
        format.html { redirect_to @content_module, notice: 'Content module was successfully created.' }
        format.json { render :show, status: :created, location: @content_module }
      else
        format.html { render :new }
        format.json { render json: @content_module.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /content_modules/1
  # PATCH/PUT /content_modules/1.json
  def update
    respond_to do |format|
      if @content_module.update(content_module_params)
        format.html { redirect_to @content_module, notice: 'Content module was successfully updated.' }
        format.json { render :show, status: :ok, location: @content_module }
      else
        format.html { render :edit }
        format.json { render json: @content_module.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /content_modules/1
  # DELETE /content_modules/1.json
  def destroy
    @content_module.destroy
    respond_to do |format|
      format.html { redirect_to content_modules_url, notice: 'Content module was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_content_module
      @content_module = ContentModule.friendly.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def content_module_params
      params.require(:content_module).permit(:meta_attribution, :meta_keywords, :meta_title, :content, :references)
    end
end
