json.extract! @content_module, :id, :meta_attribution, :meta_keywords, :meta_title, :content, :references, :created_at, :updated_at
