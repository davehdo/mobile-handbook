json.array!(@content_modules) do |content_module|
  json.extract! content_module, :id, :meta_attribution, :meta_keywords, :meta_title, :content, :references
  json.url content_module_url(content_module, format: :json)
end
