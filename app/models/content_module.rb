class ContentModule < ActiveRecord::Base
  extend FriendlyId
  
  friendly_id :meta_title, use: [:slugged, :finders]
  validates :meta_title, length: {maximum: 255}
  validates :meta_attribution, length: {maximum: 255}
  validates :meta_keywords, length: {maximum: 255}
  
end
