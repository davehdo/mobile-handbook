class CreateContentModules < ActiveRecord::Migration
  def change
    create_table :content_modules do |t|
      t.string :meta_attribution
      t.string :meta_keywords
      t.string :meta_title
      t.text :content
      t.text :references
      t.string :slug
      
      t.timestamps null: false
    end
  end
end
