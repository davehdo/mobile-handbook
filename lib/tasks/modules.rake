namespace :modules do

  task :parse_document => :environment do
    require "iconv"
        
    ContentModule.destroy_all
    
    f = File.open("./public/JAR_Guide_2015_040115.htm", "rb")
    doc = Nokogiri::HTML(Iconv.conv("UTF-8//IGNORE", "US-ASCII", f.read))
    f.close
    
    style_nodes = doc.css("style")
    
    headings = doc.css("h1")
    
    # helping images display properly
    doc.css("imagedata").add_class("img-responsive")
    doc.css("imagedata").each {|e| e.name = "img"}
    
    # removing inline styles from regular text
    doc.css("p").attr("style", "")
    doc.css("p > span").attr("style", "")

    #
    doc.css("table").add_class("table").attr("style", "")
    doc.css("table tr").attr("style", "")
    doc.css("table td").attr("style", "")
    
    headings.each do |heading_node|
      next_heading_node = headings[headings.index(heading_node) + 1]
      sibling_nodes = heading_node.parent.children
      start_index = sibling_nodes.index(heading_node)
      stop_index = sibling_nodes.index( next_heading_node ) || -1
      
      heading = heading_node.text
      section = sibling_nodes[start_index..stop_index].collect {|e| e.to_s}.join("")
      # styles = style_nodes.collect {|e| e.to_s}.join("")

      c = ContentModule.create( meta_title: heading, content: (section).gsub(/JAR_Guide_2015_040115_files/, "/JAR_Guide_2015_040115_files") )
      
      if c.new_record?
        puts "** unable to create chapter #{heading}"
      else
        puts "Created chapter #{heading}"
      end
    end
    
  end
end