# Introduction
Often there are handbooks and pocket guides produced for a specific job. There are benefits to printing physical handbooks but also to have an mobile web version. This app takes a handbook from a Word document and creates a mobile-friendly web app containing the contents. 

This is a work in progress.

# Setting up

## Prep the document
1. Go through the document and change all section headings to "Heading 1"
1. Save desired document as html with assets and place in the /public directory (e.g. JAR_Guide_2015_040115_files/ and JAR_Guide_2015_040115_files.html)

## Parse the document
1. In /lib/tasks/module.rake, update the reference to the file 
1. From the terminal, run rake modules:parse_document
1. Run the Ruby on Rails app